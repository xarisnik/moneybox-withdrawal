using System;
using Moneybox.App;
using NUnit.Framework;

namespace MoneyBox.nUnitTests
{
    [TestFixture]
    public class AccountTests
    {
        [Test]
        public void WithdrawMoney_BalanceIsLowerThanWithdrawAmount_ThrowsAnException()
        {
            var account = new Account { Balance = 100 };

            Assert.That(() => account.WithdrawMoney(101),
                Throws.TypeOf<InvalidOperationException>());
        }

        [Test]
        public void WithdrawMoney_WithdrawAmountIsNegativeNumber_ThrowsAnException()
        {
            var account = new Account {};

            Assert.That(() => account.WithdrawMoney(-10),
                Throws.TypeOf<InvalidOperationException>());
        }

        [Test]
        public void WithdrawMoney_WithdrawAmountIs100_SetBalanceTo400SetWithdrawnTo600()
        {
            var account = new Account { Balance = 500, Withdrawn = 500 };

            account.WithdrawMoney(100);

            Assert.That(account.Balance == 400);
            Assert.That(account.Withdrawn == 600);
        }

        [Test]
        public void ReceiveMoney_ReceiveAmountIsNegative_ThrowsAnException()
        {
            var account = new Account { };

            Assert.That(() => account.ReceiveMoney(-10),
                Throws.TypeOf<InvalidOperationException>());
        }


        [Test]
        public void ReceiveMoney_ReceiveAmountIsZero_ThrowsAnException()
        {
            var account = new Account { };

            Assert.That(() => account.ReceiveMoney(0m),
                Throws.TypeOf<InvalidOperationException>());
        }


        [Test]
        public void ReceiveMoney_ReceiveAmountReachesPayInLimit_ThrowsAnException()
        {
            var account = new Account { Balance = 500m, PaidIn = 3000m };

            Assert.That(() => account.ReceiveMoney(1100m),
                Throws.TypeOf<InvalidOperationException>());
        }


        [Test]
        public void ReceiveMoney_ReceiveAmountIs100_SetBalanceAndPaidInTo600()
        {
            var account = new Account { Balance = 500m, PaidIn = 500m };

            account.ReceiveMoney(100m);

            Assert.That(account.Balance == 600m);
            Assert.That(account.PaidIn == 600m);
        }
    }
}