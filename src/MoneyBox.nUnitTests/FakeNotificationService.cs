using System;
using Moneybox.App.Domain.Services;

namespace MoneyBox.nUnitTests
{
    public class FakeNotificationService : INotificationService
    {
        public void NotifyApproachingPayInLimit(string emailAddress)
        {
            throw new NotImplementedException();
        }

        public void NotifyFundsLow(string emailAddress)
        {
            throw new NotImplementedException();
        }
    }
}