using System;
using Moneybox.App.Features;
using NUnit.Framework;

namespace MoneyBox.nUnitTests
{
    [TestFixture]
    public class TransferMoneyTests
    {
        [Test]
        public void Execute_WithdrawAmountIsLessThanZero_ThrowsAnException()
        {
            var withdrawMoneyObject = new WithdrawMoney(new FakeAccountRepository(), new FakeNotificationService());

            Assert.That(() => withdrawMoneyObject.Execute(new Guid(), -1m),
                Throws.TypeOf<InvalidOperationException>());
        }
    }
}