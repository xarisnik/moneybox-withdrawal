using System;
using System.Xml.Serialization;
using Moneybox.App.Features;
using NUnit.Framework;

namespace MoneyBox.nUnitTests
{
    [TestFixture]
    public class WithdrawMoneyTests
    {
        [Test]
        public void Execute_WithdrawAmountIsLessThanZero_ThrowsAnException()
        {
            var withdrawMoneyObject = new WithdrawMoney(new FakeAccountRepository(), new FakeNotificationService());

            Assert.That(() => withdrawMoneyObject.Execute(new Guid(), -2m),
                Throws.TypeOf<InvalidOperationException>());
        }
    }
}