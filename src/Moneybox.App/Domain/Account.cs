﻿using System;

namespace Moneybox.App
{
    public class Account
    {
        public const decimal PayInLimit = 4000m;
        public const decimal FundsLowThreshold = 500m;

        public Guid Id { get; set; }

        public User User { get; set; }

        public decimal Balance { get; set; }

        public decimal Withdrawn { get; set; }

        public decimal PaidIn { get; set; }

        public void WithdrawMoney(decimal amount)
        {
            ValidateFundsToWithdraw(amount);

            Balance -= amount;
            Withdrawn += amount;
        }

        public void ReceiveMoney(decimal amount)
        {
            ValidatePayInLimit(amount);

            Balance += amount;
            PaidIn += amount;
        }

        private void ValidateFundsToWithdraw(decimal amount)
        {
            if (Balance - amount < Decimal.Zero | amount <= Decimal.Zero)
                throw new InvalidOperationException("Insufficient funds to withdraw");
        }

        private void ValidatePayInLimit(decimal amount)
        {
            if (PaidIn + amount > PayInLimit)
                throw new InvalidOperationException("Account pay in limit reached");

            if (amount <= Decimal.Zero)
                throw new InvalidOperationException("Insufficient funds to withdraw");
        }
    }
}
