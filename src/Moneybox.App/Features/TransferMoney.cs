﻿using System;
using Moneybox.App.DataAccess;
using Moneybox.App.Domain.Services;

namespace Moneybox.App.Features
{
    public class TransferMoney
    {
        private readonly IAccountRepository _accountRepository;
        private readonly INotificationService _notificationService;

        public TransferMoney(IAccountRepository accountRepository, INotificationService notificationService)
        {
            _accountRepository = accountRepository;
            _notificationService = notificationService;
        }

        public void Execute(Guid fromAccountId, Guid toAccountId, decimal amount)
        {
            var from = _accountRepository.GetAccountById(fromAccountId);
            var to = _accountRepository.GetAccountById(toAccountId);

            from.WithdrawMoney(amount);
            to.ReceiveMoney(amount);
            
            //Following two methods could be in a different class using a new 'Notification Checker' Interface for loose coupling
            FundsLowNotificationCheck(from);
            PayInLimitNotificationCheck(amount, to);

            _accountRepository.Update(from);
            _accountRepository.Update(to);
        }

        private void FundsLowNotificationCheck(Account from)
        {
            if (from.Balance < Account.FundsLowThreshold)
                _notificationService.NotifyFundsLow(from.User.Email);
        }

        private void PayInLimitNotificationCheck(decimal amount, Account to)
        {
            if (Account.PayInLimit - to.PaidIn + amount < Account.FundsLowThreshold)
                _notificationService.NotifyApproachingPayInLimit(to.User.Email);
        }
    }
}
