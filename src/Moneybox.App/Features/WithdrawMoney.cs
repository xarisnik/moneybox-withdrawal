﻿using System;
using Moneybox.App.DataAccess;
using Moneybox.App.Domain.Services;

namespace Moneybox.App.Features
{
    public class WithdrawMoney
    {
        private readonly IAccountRepository _accountRepository;
        private readonly INotificationService _notificationService;

        public WithdrawMoney(IAccountRepository accountRepository, INotificationService notificationService)
        {
            _accountRepository = accountRepository;
            _notificationService = notificationService;
        }

        public void Execute(Guid fromAccountId, decimal amount)
        {
            var from = _accountRepository.GetAccountById(fromAccountId);

            from.WithdrawMoney(amount);

            //following method could be in a different class. Like in TransferMoney class. It is not good that I created same method and implementation as the one in the TransferMoney. This is due to a time limit
            FundsLowNotificationCheck(from);

            _accountRepository.Update(from);
        }

        private void FundsLowNotificationCheck(Account from)
        {
            if (from.Balance < Account.FundsLowThreshold)
                _notificationService.NotifyFundsLow(from.User.Email);
        }
    }
}
