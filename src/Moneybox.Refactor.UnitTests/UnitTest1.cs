﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Moneybox.Refactor.UnitTests
{
    [TestClass]
    public class WithdrawMoneyTests
    {
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Execute_FromBalanceIsLessThanZero_ThrowsAnException()
        {

        }
    }
}
